if test -f "$3"; then
rm -f "$3"
fi
touch "$3"
for i in "$1"/*; do
($2 $i && echo "$i 0" >> "$3") || echo "$i 1" >> "$3"
done
