#include "shell.hh"

int main()
{ std::string input;

  // ToDo: Vervang onderstaande regel: Laad prompt uit bestand
  int fd = syscall(SYS_open, "config", O_RDONLY, 0755);
  char line[80];
  std::string prompt = std::to_string(syscall(SYS_read, fd, line, 80));
  prompt = line;
  while(true)
  { std::cout << prompt;                   // Print het prompt
    std::getline(std::cin, input);         // Lees een regel
    if (input == "new_file") new_file();   // Kies de functie
    else if (input == "ls") list();        //   op basis van
    else if (input == "src") src();        //   de invoer
    else if (input == "find") find();
    else if (input == "seek") seek();
    else if (input == "exit") return 0;
    else if (input == "quit") return 0;
    else if (input == "error") return 1;

    if (std::cin.eof()) return 0; } }      // EOF is een exit

void new_file() // ToDo: Implementeer volgens specificatie.
{ std::cout << "Geef de naam van het het bestand: "; 
  std::string filename;
  std::getline(std::cin, filename);
  std::cout << "Geef de tekst: ";
  std::string text;
  std::getline(std::cin, text);
  syscall(SYS_creat, filename.c_str(), 0755);
  int fd = syscall(SYS_open, filename.c_str(), O_RDWR, 0755);
  const char *buffer = text.c_str();
  syscall(SYS_write, fd, buffer , strlen(buffer));
  syscall(SYS_close, fd);
}

void list() // ToDo: Implementeer volgens specificatie.
{ int pid = syscall(SYS_fork);
  if(pid == 0){
    const char* args[] = {"/bin/ls", "-l", "-a", NULL};
    std::cout << syscall(SYS_execve, args[0], args, NULL);
  }
  else{
    syscall(SYS_wait4, pid, NULL, NULL, NULL);
  }
}

void find()
{ 	
  std::cout << "Geef een string: "; 
	std::string str = "";
	std::getline(std::cin, str);
	int fd[2];
	syscall(SYS_pipe, &fd, NULL);
	int PID = syscall(SYS_fork);
	if(PID == 0){
		int PID1 = syscall(SYS_fork);
		if(PID1 == 0){
			syscall(SYS_close, fd[0]);
			syscall(SYS_dup2, fd[1], 1);
			const char* args[] = { "/usr/bin/find", ".", NULL };
      syscall(SYS_execve, args[0], args, NULL);
		}
		else{
			syscall(SYS_close, fd[1]);
      syscall(SYS_dup2, fd[0], 0);
      const char* args2[] = {"/bin/grep", str.c_str(), NULL};
      syscall(SYS_execve, args2[0], args2, NULL); 
			syscall(SYS_wait4, PID1, NULL, NULL, NULL);
		}
	}
	else{  
			syscall(SYS_wait4, PID, NULL, NULL, NULL);
      return;
		}
}


void seek() { // ToDo: Implementeer volgens specificatie.
  std::string file_loop = "loop";
  std::string file_seek = "seek";

  int createseek = syscall(SYS_open, file_seek.c_str(), O_CREAT | O_RDWR | O_TRUNC, 0666);
  int writeseek = syscall(SYS_write, createseek, "x", 1);
  int lseek = syscall(SYS_lseek, createseek,5000000,SEEK_CUR);
  int writeseek2 = syscall(SYS_write, createseek, "x", 1);
 
  if ((syscall(writeseek) == 0) || (syscall(lseek) == 0) || (syscall(writeseek2) == 0)) {
     std::cout << "Lseek error" << std::endl;
  }
  

  int createloop = syscall(SYS_open, file_loop.c_str(), O_CREAT | O_RDWR | O_TRUNC, 0666);
  int writeloop0 = syscall(SYS_write, createloop, "x", 1);
  for (unsigned int i = 0; i < 5000000; i++) {
    int writeloop1 = syscall(SYS_write, createloop, "\0", 1);
  }
  int writeloop2 = syscall(SYS_write, createloop, "x", 1);
 
  if ((syscall(writeloop0) == 0) || (syscall(writeloop2) == 0)) {
     std::cout << "Loop error" << std::endl;
  }
}

void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
  char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
  while(syscall(SYS_read, fd, byte, 1))                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
    std::cout << byte; }                                  //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.
